class Guest < ApplicationRecord
	has_many :contact

	accepts_nested_attributes_for :contact
end
