class Reservation < ApplicationRecord
  belongs_to :guest
  has_many :contact, through: :guest
  accepts_nested_attributes_for :guest
end
