class ReservationSerializer < ActiveModel::Serializer
  attributes :id, :start_date, :end_date, :nights, :guests, :adults, :children, :infants, :status
  has_one :guest
  has_many :contact, through: :guest
end
