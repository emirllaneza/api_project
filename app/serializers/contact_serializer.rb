class ContactSerializer < ActiveModel::Serializer
  attributes :id, :phone
  has_one :guest
end
