# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
10.times do
	Reservation.create({"guest_attributes"=>{:first_name=>Faker::Internet.user_name, :last_name=> Faker::Internet.user_name, :email=>Faker::Internet.email ,"contact_attributes"=>[{:phone=>Faker::Number.number(digits: 10)}]},:status=>'1'})
end