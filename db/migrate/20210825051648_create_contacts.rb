class CreateContacts < ActiveRecord::Migration[6.1]
  def change
    create_table :contacts do |t|
      t.references :guest, null: false, foreign_key: true
      t.integer :phone

      t.timestamps
    end
  end
end
