class CreateReservations < ActiveRecord::Migration[6.1]
  def change
    create_table :reservations do |t|
      t.references :guest, null: false, foreign_key: true
      t.string :start_date
      t.string :end_date
      t.string :nights
      t.string :guests
      t.string :adults
      t.string :children
      t.string :infants
      t.string :status

      t.timestamps
    end
  end
end
